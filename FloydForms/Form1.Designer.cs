﻿
namespace FloydForms
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.starter_a11 = new System.Windows.Forms.TextBox();
            this.starter_a12 = new System.Windows.Forms.TextBox();
            this.starter_a13 = new System.Windows.Forms.TextBox();
            this.starter_a14 = new System.Windows.Forms.TextBox();
            this.starter_a15 = new System.Windows.Forms.TextBox();
            this.starter_a25 = new System.Windows.Forms.TextBox();
            this.starter_a24 = new System.Windows.Forms.TextBox();
            this.starter_a23 = new System.Windows.Forms.TextBox();
            this.starter_a22 = new System.Windows.Forms.TextBox();
            this.starter_a21 = new System.Windows.Forms.TextBox();
            this.starter_a35 = new System.Windows.Forms.TextBox();
            this.starter_a34 = new System.Windows.Forms.TextBox();
            this.starter_a33 = new System.Windows.Forms.TextBox();
            this.starter_a32 = new System.Windows.Forms.TextBox();
            this.starter_a31 = new System.Windows.Forms.TextBox();
            this.starter_a45 = new System.Windows.Forms.TextBox();
            this.starter_a44 = new System.Windows.Forms.TextBox();
            this.starter_a43 = new System.Windows.Forms.TextBox();
            this.starter_a42 = new System.Windows.Forms.TextBox();
            this.starter_a41 = new System.Windows.Forms.TextBox();
            this.starter_a55 = new System.Windows.Forms.TextBox();
            this.starter_a54 = new System.Windows.Forms.TextBox();
            this.starter_a53 = new System.Windows.Forms.TextBox();
            this.starter_a52 = new System.Windows.Forms.TextBox();
            this.starter_a51 = new System.Windows.Forms.TextBox();
            this.length_a55 = new System.Windows.Forms.TextBox();
            this.length_a54 = new System.Windows.Forms.TextBox();
            this.length_a53 = new System.Windows.Forms.TextBox();
            this.length_a52 = new System.Windows.Forms.TextBox();
            this.length_a51 = new System.Windows.Forms.TextBox();
            this.length_a45 = new System.Windows.Forms.TextBox();
            this.length_a44 = new System.Windows.Forms.TextBox();
            this.length_a43 = new System.Windows.Forms.TextBox();
            this.length_a42 = new System.Windows.Forms.TextBox();
            this.length_a41 = new System.Windows.Forms.TextBox();
            this.length_a35 = new System.Windows.Forms.TextBox();
            this.length_a34 = new System.Windows.Forms.TextBox();
            this.length_a33 = new System.Windows.Forms.TextBox();
            this.length_a32 = new System.Windows.Forms.TextBox();
            this.length_a31 = new System.Windows.Forms.TextBox();
            this.length_a25 = new System.Windows.Forms.TextBox();
            this.length_a24 = new System.Windows.Forms.TextBox();
            this.length_a23 = new System.Windows.Forms.TextBox();
            this.length_a22 = new System.Windows.Forms.TextBox();
            this.length_a21 = new System.Windows.Forms.TextBox();
            this.length_a15 = new System.Windows.Forms.TextBox();
            this.length_a14 = new System.Windows.Forms.TextBox();
            this.length_a13 = new System.Windows.Forms.TextBox();
            this.length_a12 = new System.Windows.Forms.TextBox();
            this.length_a11 = new System.Windows.Forms.TextBox();
            this.ways_a55 = new System.Windows.Forms.TextBox();
            this.ways_a54 = new System.Windows.Forms.TextBox();
            this.ways_a53 = new System.Windows.Forms.TextBox();
            this.ways_a52 = new System.Windows.Forms.TextBox();
            this.ways_a51 = new System.Windows.Forms.TextBox();
            this.ways_a45 = new System.Windows.Forms.TextBox();
            this.ways_a44 = new System.Windows.Forms.TextBox();
            this.ways_a43 = new System.Windows.Forms.TextBox();
            this.ways_a42 = new System.Windows.Forms.TextBox();
            this.ways_a41 = new System.Windows.Forms.TextBox();
            this.ways_a35 = new System.Windows.Forms.TextBox();
            this.ways_a34 = new System.Windows.Forms.TextBox();
            this.ways_a33 = new System.Windows.Forms.TextBox();
            this.ways_a32 = new System.Windows.Forms.TextBox();
            this.ways_a31 = new System.Windows.Forms.TextBox();
            this.ways_a25 = new System.Windows.Forms.TextBox();
            this.ways_a24 = new System.Windows.Forms.TextBox();
            this.ways_a23 = new System.Windows.Forms.TextBox();
            this.ways_a22 = new System.Windows.Forms.TextBox();
            this.ways_a21 = new System.Windows.Forms.TextBox();
            this.ways_a15 = new System.Windows.Forms.TextBox();
            this.ways_a14 = new System.Windows.Forms.TextBox();
            this.ways_a13 = new System.Windows.Forms.TextBox();
            this.ways_a12 = new System.Windows.Forms.TextBox();
            this.ways_a11 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button_calculate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(46, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Початкова матриця :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(307, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(293, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Матриця найкоротших відстаней :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(307, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(270, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Матриця найкоротших шляхів :";
            // 
            // starter_a11
            // 
            this.starter_a11.Location = new System.Drawing.Point(18, 64);
            this.starter_a11.Name = "starter_a11";
            this.starter_a11.Size = new System.Drawing.Size(44, 27);
            this.starter_a11.TabIndex = 3;
            this.starter_a11.Text = "1000";
            this.starter_a11.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // starter_a12
            // 
            this.starter_a12.Location = new System.Drawing.Point(68, 64);
            this.starter_a12.Name = "starter_a12";
            this.starter_a12.Size = new System.Drawing.Size(44, 27);
            this.starter_a12.TabIndex = 4;
            this.starter_a12.Text = "2";
            this.starter_a12.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // starter_a13
            // 
            this.starter_a13.Location = new System.Drawing.Point(118, 64);
            this.starter_a13.Name = "starter_a13";
            this.starter_a13.Size = new System.Drawing.Size(44, 27);
            this.starter_a13.TabIndex = 5;
            this.starter_a13.Text = "1000";
            this.starter_a13.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // starter_a14
            // 
            this.starter_a14.Location = new System.Drawing.Point(168, 64);
            this.starter_a14.Name = "starter_a14";
            this.starter_a14.Size = new System.Drawing.Size(44, 27);
            this.starter_a14.TabIndex = 6;
            this.starter_a14.Text = "1000";
            this.starter_a14.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // starter_a15
            // 
            this.starter_a15.Location = new System.Drawing.Point(218, 64);
            this.starter_a15.Name = "starter_a15";
            this.starter_a15.Size = new System.Drawing.Size(44, 27);
            this.starter_a15.TabIndex = 7;
            this.starter_a15.Text = "10";
            this.starter_a15.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // starter_a25
            // 
            this.starter_a25.Location = new System.Drawing.Point(218, 97);
            this.starter_a25.Name = "starter_a25";
            this.starter_a25.Size = new System.Drawing.Size(44, 27);
            this.starter_a25.TabIndex = 12;
            this.starter_a25.Text = "6";
            // 
            // starter_a24
            // 
            this.starter_a24.Location = new System.Drawing.Point(168, 97);
            this.starter_a24.Name = "starter_a24";
            this.starter_a24.Size = new System.Drawing.Size(44, 27);
            this.starter_a24.TabIndex = 11;
            this.starter_a24.Text = "1000";
            // 
            // starter_a23
            // 
            this.starter_a23.Location = new System.Drawing.Point(118, 97);
            this.starter_a23.Name = "starter_a23";
            this.starter_a23.Size = new System.Drawing.Size(44, 27);
            this.starter_a23.TabIndex = 10;
            this.starter_a23.Text = "1";
            // 
            // starter_a22
            // 
            this.starter_a22.Location = new System.Drawing.Point(68, 97);
            this.starter_a22.Name = "starter_a22";
            this.starter_a22.Size = new System.Drawing.Size(44, 27);
            this.starter_a22.TabIndex = 9;
            this.starter_a22.Text = "1000";
            // 
            // starter_a21
            // 
            this.starter_a21.Location = new System.Drawing.Point(18, 97);
            this.starter_a21.Name = "starter_a21";
            this.starter_a21.Size = new System.Drawing.Size(44, 27);
            this.starter_a21.TabIndex = 8;
            this.starter_a21.Text = "1000";
            // 
            // starter_a35
            // 
            this.starter_a35.Location = new System.Drawing.Point(218, 130);
            this.starter_a35.Name = "starter_a35";
            this.starter_a35.Size = new System.Drawing.Size(44, 27);
            this.starter_a35.TabIndex = 17;
            this.starter_a35.Text = "3";
            // 
            // starter_a34
            // 
            this.starter_a34.Location = new System.Drawing.Point(168, 130);
            this.starter_a34.Name = "starter_a34";
            this.starter_a34.Size = new System.Drawing.Size(44, 27);
            this.starter_a34.TabIndex = 16;
            this.starter_a34.Text = "1";
            // 
            // starter_a33
            // 
            this.starter_a33.Location = new System.Drawing.Point(118, 130);
            this.starter_a33.Name = "starter_a33";
            this.starter_a33.Size = new System.Drawing.Size(44, 27);
            this.starter_a33.TabIndex = 15;
            this.starter_a33.Text = "1000";
            // 
            // starter_a32
            // 
            this.starter_a32.Location = new System.Drawing.Point(68, 130);
            this.starter_a32.Name = "starter_a32";
            this.starter_a32.Size = new System.Drawing.Size(44, 27);
            this.starter_a32.TabIndex = 14;
            this.starter_a32.Text = "1000";
            // 
            // starter_a31
            // 
            this.starter_a31.Location = new System.Drawing.Point(18, 130);
            this.starter_a31.Name = "starter_a31";
            this.starter_a31.Size = new System.Drawing.Size(44, 27);
            this.starter_a31.TabIndex = 13;
            this.starter_a31.Text = "1000";
            // 
            // starter_a45
            // 
            this.starter_a45.Location = new System.Drawing.Point(218, 163);
            this.starter_a45.Name = "starter_a45";
            this.starter_a45.Size = new System.Drawing.Size(44, 27);
            this.starter_a45.TabIndex = 22;
            this.starter_a45.Text = "1";
            // 
            // starter_a44
            // 
            this.starter_a44.Location = new System.Drawing.Point(168, 163);
            this.starter_a44.Name = "starter_a44";
            this.starter_a44.Size = new System.Drawing.Size(44, 27);
            this.starter_a44.TabIndex = 21;
            this.starter_a44.Text = "1000";
            // 
            // starter_a43
            // 
            this.starter_a43.Location = new System.Drawing.Point(118, 163);
            this.starter_a43.Name = "starter_a43";
            this.starter_a43.Size = new System.Drawing.Size(44, 27);
            this.starter_a43.TabIndex = 20;
            this.starter_a43.Text = "1000";
            // 
            // starter_a42
            // 
            this.starter_a42.Location = new System.Drawing.Point(68, 163);
            this.starter_a42.Name = "starter_a42";
            this.starter_a42.Size = new System.Drawing.Size(44, 27);
            this.starter_a42.TabIndex = 19;
            this.starter_a42.Text = "1000";
            // 
            // starter_a41
            // 
            this.starter_a41.Location = new System.Drawing.Point(18, 163);
            this.starter_a41.Name = "starter_a41";
            this.starter_a41.Size = new System.Drawing.Size(44, 27);
            this.starter_a41.TabIndex = 18;
            this.starter_a41.Text = "1000";
            // 
            // starter_a55
            // 
            this.starter_a55.Location = new System.Drawing.Point(218, 196);
            this.starter_a55.Name = "starter_a55";
            this.starter_a55.Size = new System.Drawing.Size(44, 27);
            this.starter_a55.TabIndex = 27;
            this.starter_a55.Text = "1000";
            // 
            // starter_a54
            // 
            this.starter_a54.Location = new System.Drawing.Point(168, 196);
            this.starter_a54.Name = "starter_a54";
            this.starter_a54.Size = new System.Drawing.Size(44, 27);
            this.starter_a54.TabIndex = 26;
            this.starter_a54.Text = "1000";
            // 
            // starter_a53
            // 
            this.starter_a53.Location = new System.Drawing.Point(118, 196);
            this.starter_a53.Name = "starter_a53";
            this.starter_a53.Size = new System.Drawing.Size(44, 27);
            this.starter_a53.TabIndex = 25;
            this.starter_a53.Text = "1000";
            // 
            // starter_a52
            // 
            this.starter_a52.Location = new System.Drawing.Point(68, 196);
            this.starter_a52.Name = "starter_a52";
            this.starter_a52.Size = new System.Drawing.Size(44, 27);
            this.starter_a52.TabIndex = 24;
            this.starter_a52.Text = "1000";
            // 
            // starter_a51
            // 
            this.starter_a51.Location = new System.Drawing.Point(18, 196);
            this.starter_a51.Name = "starter_a51";
            this.starter_a51.Size = new System.Drawing.Size(44, 27);
            this.starter_a51.TabIndex = 23;
            this.starter_a51.Text = "1000";
            // 
            // length_a55
            // 
            this.length_a55.Location = new System.Drawing.Point(507, 196);
            this.length_a55.Name = "length_a55";
            this.length_a55.ReadOnly = true;
            this.length_a55.Size = new System.Drawing.Size(44, 27);
            this.length_a55.TabIndex = 52;
            // 
            // length_a54
            // 
            this.length_a54.Location = new System.Drawing.Point(457, 196);
            this.length_a54.Name = "length_a54";
            this.length_a54.ReadOnly = true;
            this.length_a54.Size = new System.Drawing.Size(44, 27);
            this.length_a54.TabIndex = 51;
            // 
            // length_a53
            // 
            this.length_a53.Location = new System.Drawing.Point(407, 196);
            this.length_a53.Name = "length_a53";
            this.length_a53.ReadOnly = true;
            this.length_a53.Size = new System.Drawing.Size(44, 27);
            this.length_a53.TabIndex = 50;
            // 
            // length_a52
            // 
            this.length_a52.Location = new System.Drawing.Point(357, 196);
            this.length_a52.Name = "length_a52";
            this.length_a52.ReadOnly = true;
            this.length_a52.Size = new System.Drawing.Size(44, 27);
            this.length_a52.TabIndex = 49;
            // 
            // length_a51
            // 
            this.length_a51.Location = new System.Drawing.Point(307, 196);
            this.length_a51.Name = "length_a51";
            this.length_a51.ReadOnly = true;
            this.length_a51.Size = new System.Drawing.Size(44, 27);
            this.length_a51.TabIndex = 48;
            // 
            // length_a45
            // 
            this.length_a45.Location = new System.Drawing.Point(507, 163);
            this.length_a45.Name = "length_a45";
            this.length_a45.ReadOnly = true;
            this.length_a45.Size = new System.Drawing.Size(44, 27);
            this.length_a45.TabIndex = 47;
            // 
            // length_a44
            // 
            this.length_a44.Location = new System.Drawing.Point(457, 163);
            this.length_a44.Name = "length_a44";
            this.length_a44.ReadOnly = true;
            this.length_a44.Size = new System.Drawing.Size(44, 27);
            this.length_a44.TabIndex = 46;
            // 
            // length_a43
            // 
            this.length_a43.Location = new System.Drawing.Point(407, 163);
            this.length_a43.Name = "length_a43";
            this.length_a43.ReadOnly = true;
            this.length_a43.Size = new System.Drawing.Size(44, 27);
            this.length_a43.TabIndex = 45;
            // 
            // length_a42
            // 
            this.length_a42.Location = new System.Drawing.Point(357, 163);
            this.length_a42.Name = "length_a42";
            this.length_a42.ReadOnly = true;
            this.length_a42.Size = new System.Drawing.Size(44, 27);
            this.length_a42.TabIndex = 44;
            // 
            // length_a41
            // 
            this.length_a41.Location = new System.Drawing.Point(307, 163);
            this.length_a41.Name = "length_a41";
            this.length_a41.ReadOnly = true;
            this.length_a41.Size = new System.Drawing.Size(44, 27);
            this.length_a41.TabIndex = 43;
            // 
            // length_a35
            // 
            this.length_a35.Location = new System.Drawing.Point(507, 130);
            this.length_a35.Name = "length_a35";
            this.length_a35.ReadOnly = true;
            this.length_a35.Size = new System.Drawing.Size(44, 27);
            this.length_a35.TabIndex = 42;
            // 
            // length_a34
            // 
            this.length_a34.Location = new System.Drawing.Point(457, 130);
            this.length_a34.Name = "length_a34";
            this.length_a34.ReadOnly = true;
            this.length_a34.Size = new System.Drawing.Size(44, 27);
            this.length_a34.TabIndex = 41;
            // 
            // length_a33
            // 
            this.length_a33.Location = new System.Drawing.Point(407, 130);
            this.length_a33.Name = "length_a33";
            this.length_a33.ReadOnly = true;
            this.length_a33.Size = new System.Drawing.Size(44, 27);
            this.length_a33.TabIndex = 40;
            // 
            // length_a32
            // 
            this.length_a32.Location = new System.Drawing.Point(357, 130);
            this.length_a32.Name = "length_a32";
            this.length_a32.ReadOnly = true;
            this.length_a32.Size = new System.Drawing.Size(44, 27);
            this.length_a32.TabIndex = 39;
            // 
            // length_a31
            // 
            this.length_a31.Location = new System.Drawing.Point(307, 130);
            this.length_a31.Name = "length_a31";
            this.length_a31.ReadOnly = true;
            this.length_a31.Size = new System.Drawing.Size(44, 27);
            this.length_a31.TabIndex = 38;
            // 
            // length_a25
            // 
            this.length_a25.Location = new System.Drawing.Point(507, 97);
            this.length_a25.Name = "length_a25";
            this.length_a25.ReadOnly = true;
            this.length_a25.Size = new System.Drawing.Size(44, 27);
            this.length_a25.TabIndex = 37;
            // 
            // length_a24
            // 
            this.length_a24.Location = new System.Drawing.Point(457, 97);
            this.length_a24.Name = "length_a24";
            this.length_a24.ReadOnly = true;
            this.length_a24.Size = new System.Drawing.Size(44, 27);
            this.length_a24.TabIndex = 36;
            // 
            // length_a23
            // 
            this.length_a23.Location = new System.Drawing.Point(407, 97);
            this.length_a23.Name = "length_a23";
            this.length_a23.ReadOnly = true;
            this.length_a23.Size = new System.Drawing.Size(44, 27);
            this.length_a23.TabIndex = 35;
            // 
            // length_a22
            // 
            this.length_a22.Location = new System.Drawing.Point(357, 97);
            this.length_a22.Name = "length_a22";
            this.length_a22.ReadOnly = true;
            this.length_a22.Size = new System.Drawing.Size(44, 27);
            this.length_a22.TabIndex = 34;
            // 
            // length_a21
            // 
            this.length_a21.Location = new System.Drawing.Point(308, 97);
            this.length_a21.Name = "length_a21";
            this.length_a21.ReadOnly = true;
            this.length_a21.Size = new System.Drawing.Size(44, 27);
            this.length_a21.TabIndex = 33;
            // 
            // length_a15
            // 
            this.length_a15.Location = new System.Drawing.Point(507, 64);
            this.length_a15.Name = "length_a15";
            this.length_a15.ReadOnly = true;
            this.length_a15.Size = new System.Drawing.Size(44, 27);
            this.length_a15.TabIndex = 32;
            // 
            // length_a14
            // 
            this.length_a14.Location = new System.Drawing.Point(457, 64);
            this.length_a14.Name = "length_a14";
            this.length_a14.ReadOnly = true;
            this.length_a14.Size = new System.Drawing.Size(44, 27);
            this.length_a14.TabIndex = 31;
            // 
            // length_a13
            // 
            this.length_a13.Location = new System.Drawing.Point(407, 64);
            this.length_a13.Name = "length_a13";
            this.length_a13.ReadOnly = true;
            this.length_a13.Size = new System.Drawing.Size(44, 27);
            this.length_a13.TabIndex = 30;
            // 
            // length_a12
            // 
            this.length_a12.Location = new System.Drawing.Point(357, 64);
            this.length_a12.Name = "length_a12";
            this.length_a12.ReadOnly = true;
            this.length_a12.Size = new System.Drawing.Size(44, 27);
            this.length_a12.TabIndex = 29;
            // 
            // length_a11
            // 
            this.length_a11.Location = new System.Drawing.Point(307, 64);
            this.length_a11.Name = "length_a11";
            this.length_a11.ReadOnly = true;
            this.length_a11.Size = new System.Drawing.Size(44, 27);
            this.length_a11.TabIndex = 28;
            // 
            // ways_a55
            // 
            this.ways_a55.Location = new System.Drawing.Point(507, 440);
            this.ways_a55.Name = "ways_a55";
            this.ways_a55.ReadOnly = true;
            this.ways_a55.Size = new System.Drawing.Size(44, 27);
            this.ways_a55.TabIndex = 77;
            // 
            // ways_a54
            // 
            this.ways_a54.Location = new System.Drawing.Point(457, 440);
            this.ways_a54.Name = "ways_a54";
            this.ways_a54.ReadOnly = true;
            this.ways_a54.Size = new System.Drawing.Size(44, 27);
            this.ways_a54.TabIndex = 76;
            // 
            // ways_a53
            // 
            this.ways_a53.Location = new System.Drawing.Point(407, 440);
            this.ways_a53.Name = "ways_a53";
            this.ways_a53.ReadOnly = true;
            this.ways_a53.Size = new System.Drawing.Size(44, 27);
            this.ways_a53.TabIndex = 75;
            // 
            // ways_a52
            // 
            this.ways_a52.Location = new System.Drawing.Point(357, 440);
            this.ways_a52.Name = "ways_a52";
            this.ways_a52.ReadOnly = true;
            this.ways_a52.Size = new System.Drawing.Size(44, 27);
            this.ways_a52.TabIndex = 74;
            // 
            // ways_a51
            // 
            this.ways_a51.Location = new System.Drawing.Point(307, 440);
            this.ways_a51.Name = "ways_a51";
            this.ways_a51.ReadOnly = true;
            this.ways_a51.Size = new System.Drawing.Size(44, 27);
            this.ways_a51.TabIndex = 73;
            // 
            // ways_a45
            // 
            this.ways_a45.Location = new System.Drawing.Point(507, 407);
            this.ways_a45.Name = "ways_a45";
            this.ways_a45.ReadOnly = true;
            this.ways_a45.Size = new System.Drawing.Size(44, 27);
            this.ways_a45.TabIndex = 72;
            // 
            // ways_a44
            // 
            this.ways_a44.Location = new System.Drawing.Point(457, 407);
            this.ways_a44.Name = "ways_a44";
            this.ways_a44.ReadOnly = true;
            this.ways_a44.Size = new System.Drawing.Size(44, 27);
            this.ways_a44.TabIndex = 71;
            // 
            // ways_a43
            // 
            this.ways_a43.Location = new System.Drawing.Point(407, 407);
            this.ways_a43.Name = "ways_a43";
            this.ways_a43.ReadOnly = true;
            this.ways_a43.Size = new System.Drawing.Size(44, 27);
            this.ways_a43.TabIndex = 70;
            // 
            // ways_a42
            // 
            this.ways_a42.Location = new System.Drawing.Point(357, 407);
            this.ways_a42.Name = "ways_a42";
            this.ways_a42.ReadOnly = true;
            this.ways_a42.Size = new System.Drawing.Size(44, 27);
            this.ways_a42.TabIndex = 69;
            // 
            // ways_a41
            // 
            this.ways_a41.Location = new System.Drawing.Point(307, 407);
            this.ways_a41.Name = "ways_a41";
            this.ways_a41.ReadOnly = true;
            this.ways_a41.Size = new System.Drawing.Size(44, 27);
            this.ways_a41.TabIndex = 68;
            // 
            // ways_a35
            // 
            this.ways_a35.Location = new System.Drawing.Point(507, 374);
            this.ways_a35.Name = "ways_a35";
            this.ways_a35.ReadOnly = true;
            this.ways_a35.Size = new System.Drawing.Size(44, 27);
            this.ways_a35.TabIndex = 67;
            // 
            // ways_a34
            // 
            this.ways_a34.Location = new System.Drawing.Point(457, 374);
            this.ways_a34.Name = "ways_a34";
            this.ways_a34.ReadOnly = true;
            this.ways_a34.Size = new System.Drawing.Size(44, 27);
            this.ways_a34.TabIndex = 66;
            // 
            // ways_a33
            // 
            this.ways_a33.Location = new System.Drawing.Point(407, 374);
            this.ways_a33.Name = "ways_a33";
            this.ways_a33.ReadOnly = true;
            this.ways_a33.Size = new System.Drawing.Size(44, 27);
            this.ways_a33.TabIndex = 65;
            // 
            // ways_a32
            // 
            this.ways_a32.Location = new System.Drawing.Point(357, 374);
            this.ways_a32.Name = "ways_a32";
            this.ways_a32.ReadOnly = true;
            this.ways_a32.Size = new System.Drawing.Size(44, 27);
            this.ways_a32.TabIndex = 64;
            // 
            // ways_a31
            // 
            this.ways_a31.Location = new System.Drawing.Point(307, 374);
            this.ways_a31.Name = "ways_a31";
            this.ways_a31.ReadOnly = true;
            this.ways_a31.Size = new System.Drawing.Size(44, 27);
            this.ways_a31.TabIndex = 63;
            // 
            // ways_a25
            // 
            this.ways_a25.Location = new System.Drawing.Point(507, 341);
            this.ways_a25.Name = "ways_a25";
            this.ways_a25.ReadOnly = true;
            this.ways_a25.Size = new System.Drawing.Size(44, 27);
            this.ways_a25.TabIndex = 62;
            // 
            // ways_a24
            // 
            this.ways_a24.Location = new System.Drawing.Point(457, 341);
            this.ways_a24.Name = "ways_a24";
            this.ways_a24.ReadOnly = true;
            this.ways_a24.Size = new System.Drawing.Size(44, 27);
            this.ways_a24.TabIndex = 61;
            // 
            // ways_a23
            // 
            this.ways_a23.Location = new System.Drawing.Point(407, 341);
            this.ways_a23.Name = "ways_a23";
            this.ways_a23.ReadOnly = true;
            this.ways_a23.Size = new System.Drawing.Size(44, 27);
            this.ways_a23.TabIndex = 60;
            // 
            // ways_a22
            // 
            this.ways_a22.Location = new System.Drawing.Point(357, 341);
            this.ways_a22.Name = "ways_a22";
            this.ways_a22.ReadOnly = true;
            this.ways_a22.Size = new System.Drawing.Size(44, 27);
            this.ways_a22.TabIndex = 59;
            // 
            // ways_a21
            // 
            this.ways_a21.Location = new System.Drawing.Point(307, 341);
            this.ways_a21.Name = "ways_a21";
            this.ways_a21.ReadOnly = true;
            this.ways_a21.Size = new System.Drawing.Size(44, 27);
            this.ways_a21.TabIndex = 58;
            // 
            // ways_a15
            // 
            this.ways_a15.Location = new System.Drawing.Point(507, 308);
            this.ways_a15.Name = "ways_a15";
            this.ways_a15.ReadOnly = true;
            this.ways_a15.Size = new System.Drawing.Size(44, 27);
            this.ways_a15.TabIndex = 57;
            // 
            // ways_a14
            // 
            this.ways_a14.Location = new System.Drawing.Point(457, 308);
            this.ways_a14.Name = "ways_a14";
            this.ways_a14.ReadOnly = true;
            this.ways_a14.Size = new System.Drawing.Size(44, 27);
            this.ways_a14.TabIndex = 56;
            // 
            // ways_a13
            // 
            this.ways_a13.Location = new System.Drawing.Point(407, 308);
            this.ways_a13.Name = "ways_a13";
            this.ways_a13.ReadOnly = true;
            this.ways_a13.Size = new System.Drawing.Size(44, 27);
            this.ways_a13.TabIndex = 55;
            // 
            // ways_a12
            // 
            this.ways_a12.Location = new System.Drawing.Point(357, 308);
            this.ways_a12.Name = "ways_a12";
            this.ways_a12.ReadOnly = true;
            this.ways_a12.Size = new System.Drawing.Size(44, 27);
            this.ways_a12.TabIndex = 54;
            // 
            // ways_a11
            // 
            this.ways_a11.Location = new System.Drawing.Point(307, 308);
            this.ways_a11.Name = "ways_a11";
            this.ways_a11.ReadOnly = true;
            this.ways_a11.Size = new System.Drawing.Size(44, 27);
            this.ways_a11.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(393, 495);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 23);
            this.label4.TabIndex = 78;
            this.label4.Text = "Сергієнко Д. О. КН-21-1";
            // 
            // button_calculate
            // 
            this.button_calculate.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_calculate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_calculate.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.button_calculate.Location = new System.Drawing.Point(18, 262);
            this.button_calculate.Name = "button_calculate";
            this.button_calculate.Size = new System.Drawing.Size(244, 205);
            this.button_calculate.TabIndex = 79;
            this.button_calculate.Text = "Обчислити";
            this.button_calculate.UseVisualStyleBackColor = false;
            this.button_calculate.Click += new System.EventHandler(this.button_calculate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(18, 495);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 23);
            this.label5.TabIndex = 80;
            this.label5.Text = "Час виконання :";
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_time.Location = new System.Drawing.Point(166, 495);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(0, 23);
            this.label_time.TabIndex = 81;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(606, 533);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button_calculate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ways_a55);
            this.Controls.Add(this.ways_a54);
            this.Controls.Add(this.ways_a53);
            this.Controls.Add(this.ways_a52);
            this.Controls.Add(this.ways_a51);
            this.Controls.Add(this.ways_a45);
            this.Controls.Add(this.ways_a44);
            this.Controls.Add(this.ways_a43);
            this.Controls.Add(this.ways_a42);
            this.Controls.Add(this.ways_a41);
            this.Controls.Add(this.ways_a35);
            this.Controls.Add(this.ways_a34);
            this.Controls.Add(this.ways_a33);
            this.Controls.Add(this.ways_a32);
            this.Controls.Add(this.ways_a31);
            this.Controls.Add(this.ways_a25);
            this.Controls.Add(this.ways_a24);
            this.Controls.Add(this.ways_a23);
            this.Controls.Add(this.ways_a22);
            this.Controls.Add(this.ways_a21);
            this.Controls.Add(this.ways_a15);
            this.Controls.Add(this.ways_a14);
            this.Controls.Add(this.ways_a13);
            this.Controls.Add(this.ways_a12);
            this.Controls.Add(this.ways_a11);
            this.Controls.Add(this.length_a55);
            this.Controls.Add(this.length_a54);
            this.Controls.Add(this.length_a53);
            this.Controls.Add(this.length_a52);
            this.Controls.Add(this.length_a51);
            this.Controls.Add(this.length_a45);
            this.Controls.Add(this.length_a44);
            this.Controls.Add(this.length_a43);
            this.Controls.Add(this.length_a42);
            this.Controls.Add(this.length_a41);
            this.Controls.Add(this.length_a35);
            this.Controls.Add(this.length_a34);
            this.Controls.Add(this.length_a33);
            this.Controls.Add(this.length_a32);
            this.Controls.Add(this.length_a31);
            this.Controls.Add(this.length_a25);
            this.Controls.Add(this.length_a24);
            this.Controls.Add(this.length_a23);
            this.Controls.Add(this.length_a22);
            this.Controls.Add(this.length_a21);
            this.Controls.Add(this.length_a15);
            this.Controls.Add(this.length_a14);
            this.Controls.Add(this.length_a13);
            this.Controls.Add(this.length_a12);
            this.Controls.Add(this.length_a11);
            this.Controls.Add(this.starter_a55);
            this.Controls.Add(this.starter_a54);
            this.Controls.Add(this.starter_a53);
            this.Controls.Add(this.starter_a52);
            this.Controls.Add(this.starter_a51);
            this.Controls.Add(this.starter_a45);
            this.Controls.Add(this.starter_a44);
            this.Controls.Add(this.starter_a43);
            this.Controls.Add(this.starter_a42);
            this.Controls.Add(this.starter_a41);
            this.Controls.Add(this.starter_a35);
            this.Controls.Add(this.starter_a34);
            this.Controls.Add(this.starter_a33);
            this.Controls.Add(this.starter_a32);
            this.Controls.Add(this.starter_a31);
            this.Controls.Add(this.starter_a25);
            this.Controls.Add(this.starter_a24);
            this.Controls.Add(this.starter_a23);
            this.Controls.Add(this.starter_a22);
            this.Controls.Add(this.starter_a21);
            this.Controls.Add(this.starter_a15);
            this.Controls.Add(this.starter_a14);
            this.Controls.Add(this.starter_a13);
            this.Controls.Add(this.starter_a12);
            this.Controls.Add(this.starter_a11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(624, 580);
            this.MinimumSize = new System.Drawing.Size(624, 580);
            this.Name = "Form1";
            this.Text = "Алгоритм Флойда";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox starter_a11;
        private System.Windows.Forms.TextBox starter_a12;
        private System.Windows.Forms.TextBox starter_a13;
        private System.Windows.Forms.TextBox starter_a14;
        private System.Windows.Forms.TextBox starter_a15;
        private System.Windows.Forms.TextBox starter_a25;
        private System.Windows.Forms.TextBox starter_a24;
        private System.Windows.Forms.TextBox starter_a23;
        private System.Windows.Forms.TextBox starter_a22;
        private System.Windows.Forms.TextBox starter_a21;
        private System.Windows.Forms.TextBox starter_a35;
        private System.Windows.Forms.TextBox starter_a34;
        private System.Windows.Forms.TextBox starter_a33;
        private System.Windows.Forms.TextBox starter_a32;
        private System.Windows.Forms.TextBox starter_a31;
        private System.Windows.Forms.TextBox starter_a45;
        private System.Windows.Forms.TextBox starter_a44;
        private System.Windows.Forms.TextBox starter_a43;
        private System.Windows.Forms.TextBox starter_a42;
        private System.Windows.Forms.TextBox starter_a41;
        private System.Windows.Forms.TextBox starter_a55;
        private System.Windows.Forms.TextBox starter_a54;
        private System.Windows.Forms.TextBox starter_a53;
        private System.Windows.Forms.TextBox starter_a52;
        private System.Windows.Forms.TextBox starter_a51;
        private System.Windows.Forms.TextBox length_a55;
        private System.Windows.Forms.TextBox length_a54;
        private System.Windows.Forms.TextBox length_a53;
        private System.Windows.Forms.TextBox length_a52;
        private System.Windows.Forms.TextBox length_a51;
        private System.Windows.Forms.TextBox length_a45;
        private System.Windows.Forms.TextBox length_a44;
        private System.Windows.Forms.TextBox length_a43;
        private System.Windows.Forms.TextBox length_a42;
        private System.Windows.Forms.TextBox length_a41;
        private System.Windows.Forms.TextBox length_a35;
        private System.Windows.Forms.TextBox length_a34;
        private System.Windows.Forms.TextBox length_a33;
        private System.Windows.Forms.TextBox length_a32;
        private System.Windows.Forms.TextBox length_a31;
        private System.Windows.Forms.TextBox length_a25;
        private System.Windows.Forms.TextBox length_a24;
        private System.Windows.Forms.TextBox length_a23;
        private System.Windows.Forms.TextBox length_a22;
        private System.Windows.Forms.TextBox length_a21;
        private System.Windows.Forms.TextBox length_a15;
        private System.Windows.Forms.TextBox length_a14;
        private System.Windows.Forms.TextBox length_a13;
        private System.Windows.Forms.TextBox length_a12;
        private System.Windows.Forms.TextBox length_a11;
        private System.Windows.Forms.TextBox ways_a55;
        private System.Windows.Forms.TextBox ways_a54;
        private System.Windows.Forms.TextBox ways_a53;
        private System.Windows.Forms.TextBox ways_a52;
        private System.Windows.Forms.TextBox ways_a51;
        private System.Windows.Forms.TextBox ways_a45;
        private System.Windows.Forms.TextBox ways_a44;
        private System.Windows.Forms.TextBox ways_a43;
        private System.Windows.Forms.TextBox ways_a42;
        private System.Windows.Forms.TextBox ways_a41;
        private System.Windows.Forms.TextBox ways_a35;
        private System.Windows.Forms.TextBox ways_a34;
        private System.Windows.Forms.TextBox ways_a33;
        private System.Windows.Forms.TextBox ways_a32;
        private System.Windows.Forms.TextBox ways_a31;
        private System.Windows.Forms.TextBox ways_a25;
        private System.Windows.Forms.TextBox ways_a24;
        private System.Windows.Forms.TextBox ways_a23;
        private System.Windows.Forms.TextBox ways_a22;
        private System.Windows.Forms.TextBox ways_a21;
        private System.Windows.Forms.TextBox ways_a15;
        private System.Windows.Forms.TextBox ways_a14;
        private System.Windows.Forms.TextBox ways_a13;
        private System.Windows.Forms.TextBox ways_a12;
        private System.Windows.Forms.TextBox ways_a11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_calculate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_time;
    }
}

