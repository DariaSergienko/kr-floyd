﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FloydForms
{
    public partial class Form1 : Form
    {
        const int inf = 1000;
        public static void FloydWarshall(int[] matr, int[] routes, int s)
        {
            for (int k = 0; k < s; ++k)
            {
                for (int i = 0; i < s; ++i)
                {
                    if (matr[i * s + k] == inf)
                    {
                        continue;
                    }
                    for (int j = 0; j < s; ++j)
                    {
                        int distance = matr[i * s + k] + matr[k * s + j];
                        if (matr[i * s + j] > distance)
                        {
                            matr[i * s + j] = distance;
                            routes[i * s + j] = k;
                        }
                    }
                }
            }
        } // функція алгоритму Флойда
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_calculate_Click(object sender, EventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            int[] arr = new int[25];
            GetValue(starter_a11, ref arr[0]); GetValue(starter_a12, ref arr[1]); GetValue(starter_a13, ref arr[2]); GetValue(starter_a14, ref arr[3]); GetValue(starter_a15, ref arr[4]);
            GetValue(starter_a21, ref arr[5]); GetValue(starter_a22, ref arr[6]); GetValue(starter_a23, ref arr[7]); GetValue(starter_a24, ref arr[8]); GetValue(starter_a25, ref arr[9]);
            GetValue(starter_a31, ref arr[10]); GetValue(starter_a32, ref arr[11]); GetValue(starter_a33, ref arr[12]); GetValue(starter_a34, ref arr[13]); GetValue(starter_a35, ref arr[14]);
            GetValue(starter_a41, ref arr[15]); GetValue(starter_a42, ref arr[16]); GetValue(starter_a43, ref arr[17]); GetValue(starter_a44, ref arr[18]); GetValue(starter_a45, ref arr[19]);
            GetValue(starter_a51, ref arr[20]); GetValue(starter_a52, ref arr[21]); GetValue(starter_a53, ref arr[22]); GetValue(starter_a54, ref arr[23]); GetValue(starter_a55, ref arr[24]);
            int[] routes = new int[25];
            int s = 5;
            stopwatch.Start();
            FloydWarshall(arr, routes, s);
            stopwatch.Stop();

            label_time.Text = $"{stopwatch.Elapsed} ns";

            length_a11.Text = $"{arr[0]}"; length_a12.Text = $"{arr[1]}"; length_a13.Text = $"{arr[2]}"; length_a14.Text = $"{arr[3]}"; length_a15.Text = $"{arr[4]}";
            length_a21.Text = $"{arr[5]}"; length_a22.Text = $"{arr[6]}"; length_a23.Text = $"{arr[7]}"; length_a24.Text = $"{arr[8]}"; length_a25.Text = $"{arr[9]}";
            length_a31.Text = $"{arr[10]}"; length_a32.Text = $"{arr[11]}"; length_a33.Text = $"{arr[12]}"; length_a34.Text = $"{arr[13]}"; length_a35.Text = $"{arr[14]}";
            length_a41.Text = $"{arr[15]}"; length_a42.Text = $"{arr[16]}"; length_a43.Text = $"{arr[17]}"; length_a44.Text = $"{arr[18]}"; length_a45.Text = $"{arr[19]}";
            length_a51.Text = $"{arr[20]}"; length_a52.Text = $"{arr[21]}"; length_a53.Text = $"{arr[22]}"; length_a54.Text = $"{arr[23]}"; length_a55.Text = $"{arr[24]}";

            ways_a11.Text = $"{routes[0]}"; ways_a12.Text = $"{routes[1]}"; ways_a13.Text = $"{routes[2]}"; ways_a14.Text = $"{routes[3]}"; ways_a15.Text = $"{routes[4]}";
            ways_a21.Text = $"{routes[5]}"; ways_a22.Text = $"{routes[6]}"; ways_a23.Text = $"{routes[7]}"; ways_a24.Text = $"{routes[8]}"; ways_a25.Text = $"{routes[9]}";
            ways_a31.Text = $"{routes[10]}"; ways_a32.Text = $"{routes[11]}"; ways_a33.Text = $"{routes[12]}"; ways_a34.Text = $"{routes[13]}"; ways_a35.Text = $"{routes[14]}";
            ways_a41.Text = $"{routes[15]}"; ways_a42.Text = $"{routes[16]}"; ways_a43.Text = $"{routes[17]}"; ways_a44.Text = $"{routes[18]}"; ways_a45.Text = $"{routes[19]}";
            ways_a51.Text = $"{routes[20]}"; ways_a52.Text = $"{routes[21]}"; ways_a53.Text = $"{routes[22]}"; ways_a54.Text = $"{routes[23]}"; ways_a55.Text = $"{routes[24]}";

        }

        static void GetValue(TextBox box, ref int value)
        {
            bool ok = true;
            ok = int.TryParse(box.Text, out value);
            if (!ok)
            {
                MessageBox.Show($" Помилка введення значення {box.Name}! Введіть ще раз!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                box.Text = "";
            }
        } // ф-ція для отримання значень з текстбоксів
    }
}
